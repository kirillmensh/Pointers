#include <iostream>
#include <string>

class Player
{
public:
    Player() : name(""), score(0)
    {}
    Player(std::string _name, int _score) : name(_name), score(_score)
    {}
    void SetName(std::string _name)
    {
        name = _name;
    }
    void SetScore(int _score)
    {
        score = _score;
    }
    std::string GetName()
    {
        return name;
    }
    int GetScore()
    {
        return score;
    }
private:
    std::string name;
    int score;
};

void BubbleSort(Player* l, int r) {
    if (r < 1) return;
    bool b = true;
    while (b)
    {
        b = false;
        for (int i = 0; i < r - 1; i++) 
        {
            if (l[i].GetScore() > l[i + 1].GetScore()) 
            {
                Player buf = l[i];
                l[i] = l[i + 1];
                l[i + 1] = buf;
                b = true;
            }
        }
        r--;
    }
}

int main()
{
    int TotalNumeberOfPlayers = 0;
    std::cout << "Incert total number of players: ";
    std::cin >> TotalNumeberOfPlayers;
    std::cout << '\n';
    //�������������� ������ �������
    Player* PlayersList = new Player[TotalNumeberOfPlayers];

    //������ ����� � ����
    for (int i=0; i<TotalNumeberOfPlayers; i++)
    {
        std::string name;
        int score;
        std::cout << "Incert "<< i <<" player name : ";
        std::cin >> name;
        PlayersList[i].SetName(name);
        std::cout << "Incert " << i << " player score : ";
        std::cin >> score;
        PlayersList[i].SetScore(score);
        std::cout << '\n';
    }
    //��������� ������ �������
    BubbleSort(PlayersList, TotalNumeberOfPlayers);

    //������� ��������������� ������
    for (int i = 0; i < TotalNumeberOfPlayers; i++)
    {
        std::cout << PlayersList[i].GetScore() << std::endl;
    }
    delete[] PlayersList;
}